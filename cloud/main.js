
//parse-server --appId AppHoroAppId --masterKey AppHoroMasterKey --cloud C:\Chuong\HoroV3\horoscope\cloud\main.js --port 1339:1339 --databaseURI mongodb://sockwave:iamraintraintismehoro@112.78.10.211:27017/horo?authSource=horo
//parse-server --appId AppHoroAppId --masterKey AppHoroMasterKey --cloud /Users/chuonglam/Work/Flutter/horo-v3/horoscope/cloud/main.js --port 1337:1337 --databaseURI mongodb://sockwave:iamraintraintismehoro@112.78.10.211:27017/horo?authSource=horo

Parse.Cloud.define('getDaily', async (req) => {
    const date = req.params.date;
    const user = req.user;
    const Message = Parse.Object.extend("Message");
    const query = new Parse.Query(Message);
    query.equalTo('date', date);

    var response = await query.find();
    var result = [];
    response.forEach(function(obj) {
        var v = JSON.parse(JSON.stringify(obj));
        v.like_count = 0;
        v.comment_count = 0;
        v.liked = false;

        if (v.likes !== undefined) {
            v.like_count = v.likes.length;
            v.liked = user !== undefined && v.likes.includes(user.id);
            delete v.likes;
        }
        if (v.comments !== undefined) {
            v.comment_count = v.comments.length;
            delete v.comments;
        }
        result.push(v);
    });
    return result;
});

Parse.Cloud.define('getRanks', async (req) => {
    const user = req.user;
    const limit = req.params.limit;
    const skip = req.params.skip;

    const Rank = Parse.Object.extend("Rank");
    const query = new Parse.Query(Rank);
    query.limit = limit;
    query.skip = skip;
    query.descending('createdAt');
    query.notEqualTo('active', false);
    var response = await query.find();
    var result = [];
    response.forEach(function(obj) {
        var v = JSON.parse(JSON.stringify(obj));
        v.like = 0;
        v.liked = false;
        if (v.likes !== undefined) {
            v.like = v.likes.length;
            v.liked = user !== undefined && v.likes.includes(user.id);
            delete v.likes;
        }
        if (v.commentCount === undefined) {
            v.commentCount = 0;
        }
        result.push(v);
    });
    return result;
})